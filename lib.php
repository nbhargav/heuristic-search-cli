<?php

/**
 * Date: 3/9/15
 * Time: 8:32 AM
 */
Class Levenshtein {
    function __construct($array, $needle, $distance) {
        $this->array = $array;
        $this->needle = $needle;
        $this->distance = (int)$distance;
        $this->result = [];
        $this->compute();
    }

    private function compute() {
        for ($i = 0; $i < count($this->array); $i++) {
            if(levenshtein($this->needle, $this->array[$i], 0, 1, 1) <= $this->distance){
                array_push($this->result, $this->array[$i]);
            }
        }
    }

    public function output() {
        for($i=0; $i < count($this->result); $i++){
            fwrite(STDOUT, $this->result[$i]);
        }
        fwrite(STDOUT, PHP_EOL);
    }
}