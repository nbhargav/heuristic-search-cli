<?php
/**
 * Date: 3/9/15
 * Time: 8:53 AM
 */
require_once('lib.php');

while($f = fgets(STDIN)){
    $inputs[] = $f;
}

foreach ($argv as $arg) {
    $e=explode("=",$arg);
    if(count($e)==2)
        $_GET[$e[0]]=$e[1];
    else
        $_GET[$e[0]]=0;
}

$a = new Levenshtein($inputs,$_GET['s'], $_GET['k']);
$a->output();