# Heuristic Search Project ##

## Introduction ##

A simple PHP CLI program to find string matches from a list of strings


## Usage ##

`$> php index.php s=STRING k=DISTANCE < input.txt`
 
> *s* is the input string that is matched against
 
> *k* is the difference that is allowable between the strings


## Requirements ##

- PHP5 CLI